﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BazaRozmyta
{
    public class Pilki
    {
        public Pilki( string nazwa, string rozmiar, string waga, double cena)
        {
            Nazwa = nazwa;
            Rozmiar = rozmiar;
            Waga = waga;
            Cena = cena;
        }

        public Pilki()
        {
        }


        public int ID { get; set; }
        public string Nazwa { get; set; }
        public string Rozmiar { get; set; }
        public string Waga { get; set; }
        public double Cena { get; set; }           
    }
}
