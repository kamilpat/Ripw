﻿using System;
using System.Collections.Generic;

namespace BazaRozmyta
{
    public class PilkiWiek
    {
        public PilkiWiek(int iDWiek, int iDPilki, double stopienPrzynaleznosci)
        {
            IDWiek = iDWiek;
            IDPilki = iDPilki;
            StopienPrzynaleznosci = stopienPrzynaleznosci;
        }

        public int ID { get; set; }
        public int IDWiek { get; set; }
        public int IDPilki { get; set; }
        public double StopienPrzynaleznosci { get; set; }

       
    }
}
