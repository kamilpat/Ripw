﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BazaRozmyta
{
  public class PilkiZastosowanie
    {
        public int ID { get; set; }
        public int IDZastosowanie { get; set; }
        public int IDPilki { get; set; }
        public double StopienPrzynaleznosci { get; set; }
        public PilkiZastosowanie(int iDZastosowanie, int iDPilki, double stopienPrzynaleznosci)
        {
            IDZastosowanie = iDZastosowanie;
            IDPilki = iDPilki;
            StopienPrzynaleznosci = stopienPrzynaleznosci;
        }
    }
  
}
