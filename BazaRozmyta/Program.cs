﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BazaRozmyta
{
    class Program
    {
        public static readonly Random random=new Random();
        public class MyContext : DbContext
        {
            public virtual DbSet<Pilki> Pilki { get; set; }
            public virtual DbSet<PilkiKolor> PilkiKolory { get; set; }
            public virtual DbSet<PilkiWiek> PilkiWieki { get; set; }
            public virtual DbSet<PilkiZastosowanie> PilkiZastosowania { get; set; }

            public virtual DbSet<Kolor> Kolory { get; set; }
            public virtual DbSet<Wiek> Wieki { get; set; }
            public virtual DbSet<Zastosowanie> Zastosowania { get; set; }

        }

        static void Main(string[] args)
        {
            string[] zastosowania = {"do nogi","do siatkowki","do koszykowki",
                                     "do wody", "do ping-ponga","do golfa",
                                     "do hokeja","do cwiczen"};
            string[] przedzialyWiekowe = { "0-3","3-8","9-15",
                                     "16-18", "19-26","27-42",
                                     "43-60","60+"};
            string[] kolory = { "czerwona","zielona","srebrna",
                                "fioletowa", "niebieska","zolta",
                                "szara","czarna"};






            using (var context = new MyContext())
            {
                var student = context.Pilki .SqlQuery("Select * from Pilkis where ID=@id", new SqlParameter("@id", 1))
                    .FirstOrDefault();
                Console.WriteLine(student);
                var pilka = context.Pilki.SqlQuery("Select * from Pilkis").ToList();
                foreach (var item in pilka)
                {
                    Console.WriteLine($"{item.ID} {item.Nazwa} {item.Cena} {item.Rozmiar} {item.Waga}");
                }
                var pilki = context.Pilki.SqlQuery("Select * from Pilkis join PilkiZastosowanies on PilkiZastosowanies.IDPilki=Pilkis.ID").ToList();
                var a=2;
            }

        }

        private static void CreateDatabaseWithData(string[] zastosowania, string[] przedzialyWiekowe, string[] kolory)
        {
            using (var context = new MyContext())
            {
                CreateBalls(context);
                CreateColors(context, kolory);
                CreateAges(context, przedzialyWiekowe);
                CreatePurposes(context, zastosowania);
                CreateBallColors(context);
                CreateBallAges(context);
                CreateBallPurposes(context);
            }
        }

        private static double randomDegree() => random.Next(0,101)/100.00;
            

        private static void CreateBalls(MyContext context)
        {
            var listOfBall = new List<Pilki>();
            listOfBall.Add(new Pilki("pilka do nogi", "srednia", "2kg", 80.45));
            listOfBall.Add(new Pilki("pilka nike", "srednia", "1kg", 60.39));
            listOfBall.Add(new Pilki("pilka origin", "duza", "0.5kg", 40.99));
            listOfBall.Add(new Pilki("pilka Voleyball ekspert", "srednia", "1.2kg", 125.69));
            listOfBall.Add(new Pilki("pilka dmuchana", "duza", "0.2kg", 20.10));
            listOfBall.Add(new Pilki("pilka dmuchana", "duza", "0.2kg", 29.17));
            listOfBall.Add(new Pilki("mini golf super", "mala", "0.1kg", 8.45));
            listOfBall.Add(new Pilki("ping pong extra", "mala", "0.01kg", 3.12));
            foreach (var ball in listOfBall)
            {
                context.Pilki.Add(ball);
            }
            context.SaveChanges();

        }
        private static void CreateAges(MyContext context,string [] ageRangeArray)
        {
           
            foreach (var ageRange in ageRangeArray)
            {
                context.Wieki.Add(new Wiek(ageRange));
            }
            context.SaveChanges();

        }
        private static void CreatePurposes(MyContext context,string [] purposesArray)
        {
            foreach (var ctrString in purposesArray)
            {
                context.Zastosowania.Add(new Zastosowanie(ctrString));
            }
            context.SaveChanges();
        }
        private static void CreateColors(MyContext context, string[] colorArray)
        {
            foreach (var ctrString in colorArray)
            {
                context.Kolory.Add(new Kolor(ctrString));
            }
            context.SaveChanges();
        }

        private static void CreateBallPurposes(MyContext context)
        {
            var n = context.Pilki.Count();
            var m = context.Zastosowania.Count();

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    context.PilkiZastosowania.Add(new PilkiZastosowanie(j,i, randomDegree()));
                }
            }

            context.SaveChanges();
        }
        private static void CreateBallAges(MyContext context)
        {
            var n = context.Pilki.Count();
            var m = context.Wieki.Count();

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    context.PilkiWieki.Add(new PilkiWiek(j, i, randomDegree()));
                }
            }

            context.SaveChanges();
        }
        private static void CreateBallColors(MyContext context)
        {
            var n = context.Pilki.Count();
            var m = context.Kolory.Count();

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    context.PilkiKolory.Add(new PilkiKolor(j, i, randomDegree()));
                }
            }
            context.SaveChanges();
        }
    }
}
    