﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BazaRozmyta
{
    public class PilkiKolor
    {
        public PilkiKolor(int iDKoloru, int iDPilki, double stopienPrzynaleznosci)
        {
            IDKoloru = iDKoloru;
            IDPilki = iDPilki;
            StopienPrzynaleznosci = stopienPrzynaleznosci;
        }

        public int ID { get; set; }
        public int IDKoloru { get; set; }
        public int IDPilki { get; set; }
        public double StopienPrzynaleznosci { get; set; }

    }
}
