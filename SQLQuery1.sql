﻿/*SELECT * FROM dbo.Pilkis
JOIN dbo.PilkiZastosowanies ON PilkiZastosowanies.IDPilki = Pilkis.ID
JOIN dbo.Zastosowanies ON Zastosowanies.ID = dbo.PilkiZastosowanies.IDPilki
JOIN dbo.PilkiWieks ON Pilkis.ID=dbo.PilkiWieks.IDPilki
JOIN dbo.Wieks ON Wieks.ID = PilkiWieks.IDWiek
JOIN dbo.PilkiKolors ON Pilkis.ID=dbo.PilkiKolors.IDPilki
JOIN dbo.Kolors ON Kolors.ID = PilkiKolors.IDKoloru
WHERE Pilkis.ID=2
*/

/*
SELECT * FROM dbo.Pilkis
JOIN dbo.PilkiZastosowanies ON PilkiZastosowanies.IDPilki = Pilkis.ID
JOIN dbo.Zastosowanies ON Zastosowanies.ID = dbo.PilkiZastosowanies.IDPilki
JOIN dbo.PilkiWieks ON Pilkis.ID=dbo.PilkiWieks.IDPilki
JOIN dbo.Wieks ON Wieks.ID = PilkiWieks.IDWiek
JOIN dbo.PilkiKolors ON Pilkis.ID=dbo.PilkiKolors.IDPilki
JOIN dbo.Kolors ON Kolors.ID = PilkiKolors.IDKoloru
WHERE PilkiZastosowanies.IDZastosowanie=2  /*Zastosowanie podac */
ORDER BY   PilkiZastosowanies.StopienPrzynaleznosci  DESC
*/
/*SELECT * FROM dbo.Pilkis
JOIN dbo.PilkiZastosowanies ON PilkiZastosowanies.IDPilki = Pilkis.ID
JOIN dbo.Zastosowanies ON Zastosowanies.ID = dbo.PilkiZastosowanies.IDPilki
JOIN dbo.PilkiWieks ON Pilkis.ID=dbo.PilkiWieks.IDPilki
JOIN dbo.Wieks ON Wieks.ID = PilkiWieks.IDWiek
JOIN dbo.PilkiKolors ON Pilkis.ID=dbo.PilkiKolors.IDPilki
JOIN dbo.Kolors ON Kolors.ID = PilkiKolors.IDKoloru
WHERE PilkiZastosowanies.IDZastosowanie=2  AND PilkiKolors.IDKoloru=2  AND PilkiWieks.IDWiek=2/*Zastosowanie podac */
ORDER BY   PilkiZastosowanies.StopienPrzynaleznosci  DESC,
PilkiKolors.StopienPrzynaleznosci DESC,
PilkiWieks.StopienPrzynaleznosci DESC 

*/

--SELECT dbo.Kolors.Nazwa  FROM dbo.Pilkis
--JOIN dbo.PilkiKolors ON Pilkis.ID=dbo.PilkiKolors.IDPilki
--JOIN dbo.Kolors ON Kolors.ID = PilkiKolors.IDKoloru
--WHERE  Pilkis.ID=2  AND
-- PilkiKolors.StopienPrzynaleznosci>=ALL (SELECT  PilkiKolors.StopienPrzynaleznosci FROM PilkiKolors  WHERE  PilkiKolors.IDPilki=2 ) 
--UNION SELECT dbo.Zastosowanies.Nazwa   FROM dbo.Zastosowanies
--JOIN dbo.PilkiZastosowanies ON PilkiZastosowanies.IDZastosowanie = Zastosowanies.ID
--JOIN dbo.Pilkis ON PilkiZastosowanies.IDPilki = dbo.Pilkis.ID
--WHERE  Pilkis.ID=2  AND
--PilkiZastosowanies.StopienPrzynaleznosci>=ALL (SELECT  PilkiZastosowanies.StopienPrzynaleznosci FROM PilkiZastosowanies  WHERE  PilkiZastosowanies.IDPilki=2 ) 
--UNION SELECT dbo.Wieks.Przedzial   FROM dbo.Wieks
--JOIN dbo.PilkiWieks ON PilkiWieks.IDWiek = Wieks.ID
--JOIN dbo.Pilkis ON PilkiWieks.IDPilki = dbo.Pilkis.ID
--WHERE  Pilkis.ID=2  AND
--PilkiWieks.StopienPrzynaleznosci>=ALL (SELECT  PilkiWieks.StopienPrzynaleznosci FROM PilkiWieks  WHERE  PilkiWieks.IDPilki=2 ) 

SELECT * FROM dbo.Pilkis
JOIN dbo.PilkiZastosowanies ON PilkiZastosowanies.IDPilki = Pilkis.ID
JOIN dbo.Zastosowanies ON Zastosowanies.ID = dbo.PilkiZastosowanies.IDPilki
JOIN dbo.PilkiWieks ON Pilkis.ID=dbo.PilkiWieks.IDPilki
JOIN dbo.Wieks ON Wieks.ID = PilkiWieks.IDWiek
JOIN dbo.PilkiKolors ON Pilkis.ID=dbo.PilkiKolors.IDPilki
JOIN dbo.Kolors ON Kolors.ID = PilkiKolors.IDKoloru
WHERE PilkiZastosowanies.IDZastosowanie=2  /*Zastosowanie podac */
ORDER BY   PilkiZastosowanies.StopienPrzynaleznosci  DESC

