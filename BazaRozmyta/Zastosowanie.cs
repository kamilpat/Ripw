﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BazaRozmyta
{
   public class Zastosowanie
    {
        public Zastosowanie(string nazwa)
        {
            Nazwa = nazwa;
        }

        public int ID { get; set; }
        public string Nazwa { get; set; }
    }
}
