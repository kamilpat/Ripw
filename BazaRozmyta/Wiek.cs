﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BazaRozmyta
{
  public class Wiek
    {
        public Wiek(string przedzial)
        {
            Przedzial = przedzial;
        }

        public int ID { get; set; }       
        public string Przedzial { get; set; }
    }
}
